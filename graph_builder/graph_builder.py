import json

def compute_drug_vertices(drugs):
    """computes vertices for drug vertices"""
    vertices=[]
    list_drugs=list(set(list(drugs["drug"])))
    for drug in list_drugs:
        vertices+=[{"value":drug,"label":"drug"}]
    return(vertices)

def compute_clinical_trials_vertices(clinical_trials):
    """computes vertices for clinical trial vertices"""
    vertices=[]
    list_title_clinical_trials=list(set(list(clinical_trials["scientific_title"])))
    for title_clinical_trials in list_title_clinical_trials:
        vertices+=[{"value":title_clinical_trials,"label":"clinical_trials"}]
    return(vertices)

def compute_pubmed_vertices(pubmed):
    """computes vertices for pubmed vertices"""
    vertices=[]
    list_title_pubmed=list(set(list(pubmed["title"])))
    for title_pubmed in list_title_pubmed:
        vertices+=[{"value":title_pubmed,"label":"pubmed"}]
    return(vertices)

def compute_journal_vertices(pubmed,clinical_trials):
    """computes vertices from clinical trial and pubmed table for journal vertices"""
    vertices=[]
    list_journals=list(set(list(pubmed["journal"])+list(clinical_trials["journal"])))
    for journal in list_journals:
        vertices+=[{"value":journal,"label":"journal"}]
    return(vertices)

def vertices_extractor(drugs,clinical_trials,pubmed):
    """function agregates all vertices"""
    return(compute_drug_vertices(drugs)+compute_clinical_trials_vertices(clinical_trials)+compute_pubmed_vertices(pubmed)+compute_journal_vertices(pubmed,clinical_trials))



def compute_drug_table_edge(drug,table_drug,idx,title):
    """computes edge between table name and drug"""
    return({"input_edge":{"value":drug,"label":"drug"},"output_edge":{"value":table_drug[title][idx],"label":"clinical_trials"},"label":table_drug["date"][idx]})

def compute_drug__pubmed_journal_edge(drug,table_drug,idx):
    """computes edges between journal and drug"""
    return({"input_edge":{"value":drug,"label":"drug"},"output_edge":{"value":table_drug["journal"][idx],"label":"journal"},"label":table_drug["date"][idx]})


def compute_edges(table,title,drug):
    """for a drug computes all edges"""
    local_edges=[]
    table_drug=table[table[title].apply(lambda r:True if drug.lower() in r.lower() else False)]
    if len(table_drug)>0:
        for idx in list(table_drug.index):
            local_edges+=[compute_drug_table_edge(drug,table_drug,idx,title),compute_drug__pubmed_journal_edge(drug,table_drug,idx)]
    return(local_edges)

def iterate_drugs(list_drugs,clinical_trials,pubmed):
    """function that iterates over drugs"""
    edges=[]
    for drug in list_drugs:
        edges+=compute_edges(pubmed,"title",drug)+compute_edges(clinical_trials,"scientific_title",drug)
    return(edges)

def edges_extractor(drugs,clinical_trials,pubmed):
    """extracts edges"""
    list_drugs=list(set(list(drugs["drug"])))
    return(iterate_drugs(list_drugs,clinical_trials,pubmed))

def build_graph(drugs,clinical_trials,pubmed):
    """function to agregate vertices and edges into a variable"""
    return({"vertices":vertices_extractor(drugs,clinical_trials,pubmed),"edges":edges_extractor(drugs,clinical_trials,pubmed)})


def save_graph(output_graph,path):
    """function to save graph into json file"""
    with open(path, 'w') as fp:
        json.dump(output_graph, fp,  indent=4)
