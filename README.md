Les réponses au test techniques se trouvent dans Compte_Rendu_TT_SP.ipynb

Ce notebook utilise la librairie créée pour le test graph_builder

Le code utilise python 3.6

le json de final se trouve dans le dossier output

les données sont dans le dossier data

Les requêtes sql sont dans un fichier text à part à cause de l'indentation sur les notebooks sur gitlab:
RequêtesSql.txt 

Si vous créez une image docker à partir du Dockerfile, ne pas oublier de faire le port forwarding pour visualiser le notebook(sudo docker run -it -p 8888:8888 image_docker)
